import unittest
from palindrome import palindrome

class TestPalindrome(unittest.TestCase):
    def test_a_palindrome_should_return_boolean(self):
        self.assertIsInstance(palindrome("eye"), bool)

    def test_b_eye_is_palindrome(self):
        self.assertTrue(palindrome("eye"))

    def test_c__eye_is_palindrome(self):
        self.assertTrue(palindrome("_eye"))

    def test_d_race_car_is_palindrome(self):
        self.assertTrue(palindrome("race car"))

    def test_e_not_a_palindrome_is_not_palindrome(self):
        self.assertFalse(palindrome("not a palindrome"))

    def test_f_a_man_is_not_palindrome(self):
        self.assertFalse(palindrome("A man, a plan, a canal. Panama"))

    def test_g_never_odd_or_even_is_palindrome(self):
        self.assertTrue(palindrome("never odd or even"))

    def test_h_nope_is_not_palindrome(self):
        self.assertFalse(palindrome("nope"))

    def test_i_almostomla_is_palindrome(self):
        self.assertTrue(palindrome("almostomla"))

    def test_j_my_age_is_palindrome(self):
        self.assertTrue(palindrome("My age is 0, 0 si ega ym."))

    def test_k_eye_for_eye_is_not_palindrome(self):
        self.assertFalse(palindrome("1 eye for of 1 eye."))

    def test_l_0_0_is_palindrome(self):
        self.assertTrue(palindrome("0_0 (: /-\\ :) 0-0"))

    def test_m_five_four_is_not_palindrome(self):
        self.assertFalse(palindrome("five|\\_/|four"))