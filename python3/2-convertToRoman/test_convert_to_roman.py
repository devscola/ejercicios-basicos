import unittest
from convert_to_roman import convertToRoman

class TestConvertToRoman(unittest.TestCase):
    def test_a_convert_2_to_II(self):
        self.assertEqual(convertToRoman(2), "II")
        
    def test_b_convert_3_to_III(self):
        self.assertEqual(convertToRoman(3), "III")
        
    def test_c_convert_4_to_IV(self):
        self.assertEqual(convertToRoman(4), "IV")
        
    def test_d_convert_5_to_V(self):
        self.assertEqual(convertToRoman(5), "V")
        
    def test_e_convert_9_to_IX(self):
        self.assertEqual(convertToRoman(9), "IX")
        
    def test_f_convert_12_to_XII(self):
        self.assertEqual(convertToRoman(12), "XII")
        
    def test_g_convert_16_to_XVI(self):
        self.assertEqual(convertToRoman(16), "XVI")
        
    def test_h_convert_29_to_XXIX(self):
        self.assertEqual(convertToRoman(29), "XXIX")
        
    def test_i_convert_44_to_XLIV(self):
        self.assertEqual(convertToRoman(44), "XLIV")
        
    def test_j_convert_45_to_XLV(self):
        self.assertEqual(convertToRoman(45), "XLV")
        
    def test_k_convert_68_to_LXVIII(self):
        self.assertEqual(convertToRoman(68), "LXVIII")
        
    def test_l_convert_83_to_LXXXIII(self):
        self.assertEqual(convertToRoman(83), "LXXXIII")
        
    def test_m_convert_97_to_XCVII(self):
        self.assertEqual(convertToRoman(97), "XCVII")
        
    def test_n_convert_99_to_XCIX(self):
        self.assertEqual(convertToRoman(99), "XCIX")
        
    def test_o_convert_400_to_CD(self):
        self.assertEqual(convertToRoman(400), "CD")
        
    def test_p_convert_500_to_D(self):
        self.assertEqual(convertToRoman(500), "D")
        
    def test_q_convert_501_to_DI(self):
        self.assertEqual(convertToRoman(501), "DI")
        
    def test_r_convert_649_to_DCXLIX(self):
        self.assertEqual(convertToRoman(649), "DCXLIX")
        
    def test_s_convert_798_to_DCCXCVIII(self):
        self.assertEqual(convertToRoman(798), "DCCXCVIII")
        
    def test_t_convert_891_to_DCCCXCI(self):
        self.assertEqual(convertToRoman(891), "DCCCXCI")
        
    def test_u_convert_1000_to_M(self):
        self.assertEqual(convertToRoman(1000), "M")
        
    def test_v_convert_1004_to_MIV(self):
        self.assertEqual(convertToRoman(1004), "MIV")
        
    def test_w_convert_1006_to_MVI(self):
        self.assertEqual(convertToRoman(1006), "MVI")
        
    def test_x_convert_1023_to_MXXIII(self):
        self.assertEqual(convertToRoman(1023), "MXXIII")
        
    def test_y_convert_2014_to_MMXIV(self):
        self.assertEqual(convertToRoman(2014), "MMXIV")
        
    def test_z_convert_3999_to_MMMCMXCIX(self):
        self.assertEqual(convertToRoman(3999), "MMMCMXCIX")