# Ejercicios básicos de programación

Elige en que lenguaje quieres hacer los ejercicios. Si quieres, ¡también puedes probarlo con diferentes lenguajes!

Los ejercicios están en orden de dificultad. Puedes mirar el archivo de tests para ver todo lo que se va a comprobar. De todos modos, intenta no resolver todo el ejercicio de golpe, empieza por el primer test y ves avanzando poco a poco.

Si te atascas, dentro de la carpeta de cada ejercicio encontrarás un archivo con pistas que te pueden ayudar a avanzar.

## Lanzando los tests

Entra en la carpeta del ejercicio y ejecuta el siguiente comando en la terminal:

`./run-tests.sh`